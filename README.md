### A utility to patch VirtualBox .vdi files ###

This is a quick C app I made to write content directly to VirtualBox disk image (.vdi) files.

**Usage:** patch_vdi disk.vdi patch.bin

Some documentation on the structure of a .vdi file: https://forums.virtualbox.org/viewtopic.php?t=8046

### What can patch_vdi do? ###

* Currently works only with **fixed size** virtual disks, do not try it with a dynamical growing disk!
* Use it to write content directly to the sector(s) of the virtual disk image
* Currently writes the entire contents  of the .bin file, starting at sector 0, the first sector of the virtual disk image

### You will need ###

* VirtualBox - tested on 4.3.36_Debian r105129
* Configured with a **fixed size** .vdi virtual disk
* To build, just run gcc
* Alternatively, included in the repo is a Code::Blocks project file
* Nasm or similar to generate a .bin file to write to the virtual disk

### Writing content to the virtual disk ###

* Attempting to boot a VirtualBox machine with a newly created disk will result in a 'No bootable medium found' error
* A hex editor will show the .vdi file is full of zeroes
* Generate a disk image containing suitable content: nasm -f bin -o bootsect.bin bootsect.asm
* Patch the virtual disk: patch_vdi BootloaderTestDrive1.vdi bootsect.bin
* Booting from the disk now shows a friendly greeting


```
#! Sample output:

Patching BootloaderTestDrive1.vdi with bootsect.bin
Disk image starting at offset 2097152 [0x00200000] bytes
VDI file is 6291456 [0x00600000] bytes
Disk image size 4194304 [0x00400000] bytes
Patch file is 512 [0x00000200] bytes
Patching VDI file...
512 bytes patched.
```

### Use at your own risk ###

Obviously a tool of this nature could render the filesystem contained within the .vdi file useless if used improperly. I cannot be responsible for what you do to your system. Always make a backup!

### Questions or Comments ###

* Visit my blog
http://www.aupcgroup.com/blog/index.php?/archives/18-Utility-to-patch-VirtualBox-.vdi-files.html
