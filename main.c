#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define _4M 4194304

uint8_t buffer[_4M];

int main(int argc, char* argv[])
{
    if (argc < 3) {
        printf("Usage: patch_vdi disk.vdi patch.bin\n");
        return 1;
    }
    printf("Patching %s with %s\n", argv[1], argv[2]);
    //
    FILE *vdifile;
    vdifile = fopen(argv[1],"r+b");     // open for reading and writing
    if (!vdifile) {
        printf("Unable to open %s", argv[1]);
        return 1;
    }
    // read the first 4M
    fread(&buffer, sizeof(uint8_t), _4M, vdifile);
    // 32 bit integer @ 0x0158 gives us the starting offset of the virtual disk image
    // or is it a 64 bit integer?
    int *startoffset = (int *)&buffer[0x0158];
    // todo: need to find information on the .vdi file structure
    // https://forums.virtualbox.org/viewtopic.php?t=8046
    // for now, just refuse to operate if the startoffset byte doesn't match my version
    long int *tmpoffset = (long int *)&buffer[0x0158];
    if ((*startoffset != 0x00200000) || (*startoffset != *tmpoffset)) {
        printf("Unable to process .vdi file, aborting...\n");
        printf("%ld", *tmpoffset);
        return *startoffset;
    }
    printf("Disk image starting at offset %d [0x%08x] bytes\n", *startoffset, *startoffset);
    // determine the size of the vdi file
    fseek(vdifile, 0L, SEEK_END);
    long int vdisize = ftell(vdifile);
    printf("VDI file is %ld [0x%08lx] bytes\n", vdisize, vdisize);
    fseek(vdifile, *startoffset, SEEK_SET);
    // check the image to patch vdi file with is small enough to fit
    long int imagesize = vdisize - *startoffset;
    printf("Disk image size %ld [0x%08lx] bytes\n", imagesize, imagesize);
    //
    FILE *patchfile;
    patchfile = fopen(argv[2],"rb");
    if (!patchfile) {
        printf("Unable to open %s\n", argv[2]);
        fclose(vdifile);
        return 1;
    }
    fseek(patchfile, 0L, SEEK_END);
    long int patchsize = ftell(patchfile);
    printf("Patch file is %ld [0x%08lx] bytes\n", patchsize, patchsize);
    fseek(patchfile, 0L, SEEK_SET);
    if (patchsize <= imagesize) {
        printf("Patching VDI file...\n");
        long int totalBytesRead = 0;
        long int bytesRead = 0;
        while (!feof(patchfile)) {
            bytesRead += fread(&buffer, sizeof(uint8_t), _4M, patchfile);
            fwrite(&buffer, sizeof(uint8_t), bytesRead, vdifile);
            totalBytesRead += bytesRead;
        }
        printf("%ld bytes patched.\n", totalBytesRead);
    }
    else {
        printf("VDI file is too small to be patched.\n");
    }
    fclose(patchfile);
    fclose(vdifile);
    return 0;
}
